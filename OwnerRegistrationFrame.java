package main;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class OwnerRegistrationFrame {
	
	private JFrame ownerFrame;
	
	public OwnerRegistrationFrame () {
		createWindow();	
		createGUI();
	}
	
	private void createWindow() {
		
		ownerFrame = new JFrame();
		ownerFrame.setSize(800,800);
		ownerFrame.setLocationRelativeTo(null);
		ownerFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ownerFrame.setTitle("Owner Dashboard.");
		ownerFrame.setVisible(true);
		
	}
	
	private void createGUI() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());;
		
		//Initial Panel
		Border blackline = BorderFactory.createEmptyBorder(15,15,10,10);
		JPanel initialPanel = new JPanel();	
		
		initialPanel.setLayout(new BorderLayout());
		initialPanel.setBorder(blackline);
		
		JButton button = new JButton("Log Out");
		JLabel label = new JLabel ("Owner Dashboard");
		label.setFont(new Font("", Font.PLAIN, 20));
		
		initialPanel.add(label, BorderLayout.WEST);
		initialPanel.add(button, BorderLayout.EAST);
		//Initial Panel
		
		//Secondary Panel
		Border secondLine = BorderFactory.createTitledBorder("Vehicle 1");
		JPanel firstVehiclePanel = new JPanel();
		
		firstVehiclePanel.setLayout(new BorderLayout());
		firstVehiclePanel.setBorder(secondLine);
		
		JPanel firstVPanel = new JPanel();
		firstVPanel.setLayout(new FlowLayout());
		firstVPanel.setBorder(BorderFactory.createEmptyBorder());
		JPanel secondVPanel = new JPanel();
		secondVPanel.setLayout(new FlowLayout());
		secondVPanel.setBorder(BorderFactory.createEmptyBorder());
		
		JLabel startLabel = new JLabel("Start");
		JTextField dateText = new JTextField("Date",15);
		JTextField timeText = new JTextField("Time",15);
		
		JLabel endLabel = new JLabel("End");
		JTextField secondaryDateText = new JTextField("Date",15);
		JTextField secondaryTimeText = new JTextField("Time",15);
		
		firstVPanel.add(startLabel);
		firstVPanel.add(dateText);
		firstVPanel.add(timeText);
		
		secondVPanel.add(endLabel);
		secondVPanel.add(secondaryDateText);
		secondVPanel.add(secondaryTimeText);
		
		firstVehiclePanel.add(firstVPanel, BorderLayout.PAGE_START);
		firstVehiclePanel.add(secondVPanel, BorderLayout.PAGE_END);
		//Secondary Panel
		
		//Third Panel
		
		Border thirdLine = BorderFactory.createTitledBorder("Vehicle 2");
		JPanel secondVehiclePanel = new JPanel();
		
		secondVehiclePanel.setLayout(new BorderLayout());
		secondVehiclePanel.setBorder(thirdLine);
		
		JPanel thirdVPanel = new JPanel();
		thirdVPanel.setLayout(new FlowLayout());
		thirdVPanel.setBorder(BorderFactory.createEmptyBorder());
		JPanel fourthVPanel = new JPanel();
		fourthVPanel.setLayout(new FlowLayout());
		fourthVPanel.setBorder(BorderFactory.createEmptyBorder());
		
		JLabel secondStartLabel = new JLabel("Start");
		JTextField secondDateText = new JTextField("Date",15);
		JTextField secondTimeText = new JTextField("Time",15);
		
		JLabel secondEndLabel = new JLabel("End");
		JTextField secondaryEndDateText = new JTextField("Date",15);
		JTextField secondaryEndTimeText = new JTextField("Time",15);
		
		thirdVPanel.add(secondStartLabel);
		thirdVPanel.add(secondDateText);
		thirdVPanel.add(secondTimeText);
		
		fourthVPanel.add(secondEndLabel);
		fourthVPanel.add(secondaryEndDateText);
		fourthVPanel.add(secondaryEndTimeText);
		
		secondVehiclePanel.add(thirdVPanel, BorderLayout.PAGE_START);
		secondVehiclePanel.add(fourthVPanel, BorderLayout.PAGE_END);
		
		//Third Panel
		
		panel.add(initialPanel, BorderLayout.PAGE_START);
		panel.add(firstVehiclePanel, BorderLayout.CENTER);
		panel.add(secondVehiclePanel, BorderLayout.PAGE_END);
		ownerFrame.add(panel);
		
	}
	
	public static void main (String [] args) {
		new OwnerRegistrationFrame();
	}
	
	
}
