import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class VehicleRegistrationFrame {
  public static void main(String[] args) {
    VehicleRegistrationFrame vehicleRegistration = new VehicleRegistrationFrame();
    vehicleRegistration.show();
  }
  
  private JFrame frame;

  public VehicleRegistrationFrame() {
    frame = new JFrame("Vehicle Registration");
    createWindow();
    createGUI();
  }

  public void show() {
    frame.setVisible(true);
  }

  private void createWindow() {
    frame.setSize(400, 400);
    frame.setLocationRelativeTo(null);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private void createGUI() {
    // Full panel
    JPanel panel = new JPanel();
    panel.setLayout(new FlowLayout());

    // Owner ID Input Panel
    JPanel ownerIdPanel = new JPanel();
    ownerIdPanel.setLayout(new FlowLayout());
    JLabel ownerIdLabel = new JLabel("Owner ID");
    JTextField ownerIdTxt = new JTextField(15);
    // Button to generate owner ID
    JButton ownerIdGenBtn = new JButton("generate");
    ownerIdPanel.add(ownerIdLabel);
    ownerIdPanel.add(ownerIdTxt);
    ownerIdPanel.add(ownerIdGenBtn);
    panel.add(ownerIdPanel);

    // Vehicel Name Input Panel
    JPanel vehicleNamePanel = new JPanel();
    vehicleNamePanel.setLayout(new FlowLayout());
    JLabel vehicleNameLabel = new JLabel("Vehicle Name");
    JTextField vehicleNameTxt = new JTextField(21);
    vehicleNamePanel.add(vehicleNameLabel);
    vehicleNamePanel.add(vehicleNameTxt);
    panel.add(vehicleNamePanel);

    // Start datetime Input Panel
    JPanel startPanel = new JPanel();
    startPanel.setLayout(new FlowLayout());
    JLabel startLabel = new JLabel("Start");
    JTextField startDate = new JTextField("Date", 12);
    startDate.setForeground(Color.GRAY);
    setTextboxPlaceholder(startDate, "Date");
    JTextField startTime = new JTextField("Time", 12);
    startTime.setForeground(Color.GRAY);
    setTextboxPlaceholder(startTime, "Time");
    startPanel.add(startLabel);
    startPanel.add(startDate);
    startPanel.add(startTime);
    panel.add(startPanel);

    // End datetime Input Panel
    JPanel endPanel = new JPanel();
    endPanel.setLayout(new FlowLayout());
    JLabel endLabel = new JLabel("End");
    JTextField endDate = new JTextField("Date", 12);
    endDate.setForeground(Color.GRAY);
    setTextboxPlaceholder(endDate, "Date");
    JTextField endTime = new JTextField("Time", 12);
    endTime.setForeground(Color.GRAY);
    setTextboxPlaceholder(endTime, "Time");
    endPanel.add(endLabel);
    endPanel.add(endDate);
    endPanel.add(endTime);
    panel.add(endPanel);

    // Panel for optional vehicle inputs
    JPanel optionalPanel = new JPanel();

    JPanel vehicleMakePanel = new JPanel();
    vehicleMakePanel.setLayout(new FlowLayout());
    JLabel vehicleMakeLabel = new JLabel("Make");
    JTextField vehicleMakeTxt = new JTextField(15);
    vehicleMakePanel.add(vehicleMakeLabel);
    vehicleMakePanel.add(vehicleMakeTxt);
    optionalPanel.add(vehicleMakePanel);

    JPanel vehicleModelPanel = new JPanel();
    vehicleModelPanel.setLayout(new FlowLayout());
    JLabel vehicleModelLabel = new JLabel("Model");
    JTextField vehicleModelTxt = new JTextField(15);
    vehicleModelPanel.add(vehicleModelLabel);
    vehicleModelPanel.add(vehicleModelTxt);
    optionalPanel.add(vehicleModelPanel);

    JPanel vehicleYearPanel = new JPanel();
    vehicleYearPanel.setLayout(new FlowLayout());
    JLabel vehicleYearLabel = new JLabel("Year");
    JTextField vehicleYearTxt = new JTextField(15);
    vehicleYearPanel.add(vehicleYearLabel);
    vehicleYearPanel.add(vehicleYearTxt);
    optionalPanel.add(vehicleYearPanel);

    optionalPanel.setBorder(BorderFactory.createTitledBorder("Optional"));
    optionalPanel.setPreferredSize(new Dimension(350, 160));
    panel.add(optionalPanel);

    // Submit button panel
    JPanel submitPanel = new JPanel();
    JButton submitBtn = new JButton("Submit");
    submitPanel.add(submitBtn);
    panel.add(submitPanel);

    frame.add(panel);
  }

  private static void setTextboxPlaceholder(JTextField textbox, String placeholder) {
    textbox.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent e) {
        if (textbox.getText().equals(placeholder)) {
          textbox.setText("");
          textbox.setForeground(Color.BLACK);
        }
      }
      @Override
      public void focusLost(FocusEvent e) {
        if (textbox.getText().isEmpty()) {
          textbox.setForeground(Color.GRAY);
          textbox.setText(placeholder);
        }
      }
    });
  }
}
